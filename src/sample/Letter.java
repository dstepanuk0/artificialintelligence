package sample;

import java.util.Random;

public class Letter {
    private int sum ;
    private String name ;
    private int d;
    private int e;
    private int y;
    private double n;
    private double[] weight = new double[101];

    public int getE() {
        return e;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getD() {
        return d;
    }

    public void setD(int d) {
        this.d = d;
    }

    public double getN() {
        return n;
    }

    public void setN(double n) {
        this.n = n;
    }

    Letter(String name, int d) {
        this.name = name;
        this.d = d;
    }

    Letter(String name) {
        this.name = name;
    }

    {
        Random random = new Random();
        for(int i =0;i<101;i++){
            weight[i] = random.nextInt(7)+1;
        }
        n = 0.8;
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private void eps(){
        e = d - y;
    }

    private void resultY(){
        if(sum >= 0)
            y = 1;
        else
            y = 0;
    }

    private void sum(int[] input){
        sum = 0;
        for(int i =0;i<101;i++){
            sum += input[i]*weight[i];
        }
    }

    private void decW(int[] input){
        double[] dW = new double[101];
        for (int i =0;i<101;i++){
            dW[i] = n*e*input[i];
            weight[i] += dW[i];
        }
    }

    void learn(int[] input, int d){
        this.d = d;
        sum(input);
        resultY();
        eps();
        if(e != 0){
            decW(input);
            learn(input,d);
        }
    }

    int result(int[] input){
        sum(input);
        resultY();
//        eps();
//        System.out.println(e + y + " d");
        return y ;
    }

    @Override
    public boolean equals(Object s) {

        if (s.equals(name)) return true;
        else
            return false;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + name.hashCode();
        return result;
    }

}
