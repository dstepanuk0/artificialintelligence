package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

public class Controller implements Initializable {
    private GraphicsContext gcB;
    private double lastX;
    private double lastY;
    private double oldX;
    private double oldY;

    @FXML private ColorPicker colorPick;
    @FXML private Canvas TheCanvas;
    @FXML private Label res;
    @FXML TextField name = new TextField();

    private static int size = 101;
    private int[] input = new int[size];
    private int d;
    private Set<Letter> letters = new HashSet<>();

    {
        setInputByDefault();
    }

    @FXML
    private void learn(){
        if(!letters.contains(new Letter(name.getText()))){
            Letter letter = new Letter(name.getText(),1);
            letter.learn(input,1);
            letters.add(letter);
            if(letters.size() > 1) {
                learnRec();
            }
        }
        else
        {
            d=0;
            learnRec();
        }

    }

    private void learnRec() {
        for (Letter l : letters) {
            if (l.getName().equals(name.getText())) {
                d = 1;
            }
            l.learn(input, d);
            d = 0;
        }
    }

    @FXML
    private void result(){
        for (Letter l :letters) {
            if(l.result(input) == 1){
                res.setText(l.getName());
            }
        }
    }

    private void setInputByDefault(){
        for(int i =0;i<size;i++){
            input[i] = 0;
        }
        input[100] = 1;
    }

    @FXML
    private void onMousePressedListener(MouseEvent e){
        this.oldX = e.getX();
        this.oldY = e.getY();
    }

    @FXML
    private void onMouseDraggedListener(MouseEvent e){
        this.lastX = e.getX();
        this.lastY = e.getY();
        freeDrawing();
    }

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>Draw methods<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    private void freeDrawing()
    {
        gcB.setStroke(colorPick.getValue());
        gcB.setLineWidth(3);
        gcB.strokeLine(oldX, oldY, lastX, lastY);
        oldX = lastX;
        oldY = lastY;
        if(oldX >= 0 && oldY >= 0 && oldX<350 && oldY<350) {
            int xy;
            int x ,y;
            x = (int) (oldX / 35) + 1;
            y = (int) (oldY / 35) + 1;
            xy = 10*(y-1) + x;
            input[xy - 1] = 1;
        }
    }

    @FXML
    private void clearCanvas(ActionEvent e)
    {
        gcB.clearRect(0, 0, TheCanvas.getWidth(), TheCanvas.getHeight());
        setInputByDefault();
    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        gcB = TheCanvas.getGraphicsContext2D();
    }
}
